// Anagram Solution: Histogram

function areAnagrams(first, second) {
  if (first.length != second.length) return false;

  let freq = [];

  console.log(first);
  first = first.split('').map(a => a.charCodeAt(0) - 97);
  second = second.split('').map(a => a.charCodeAt(0) - 97);

  console.log(first);

  first.forEach(n => (freq[n] ? freq[n]++ : (freq[n] = 1)));

  console.log(freq);

  let flag;
  second.forEach(n => {
    if (freq[n] === 0) flag = 1;
    else freq[n]--;
  });

  return flag ? false : true;
}

console.log(
  areAnagrams('mmmotherinlaw', 'mmhitlerwoman') ? 'anagram' : '!anagram'
);
